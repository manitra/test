﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Test.Console
{
    class Program
    {

        static void Main(string[] args)
        {
            System.Console.WriteLine(itoa(25, 10));
            System.Console.WriteLine(itoa(31, 16));
            System.Console.WriteLine(itoa(-25, 16));
        }

        static string itoa(int number, int b)
        {
            var positive = number >= 0;
            var result = new StringBuilder();
            number = Math.Abs(number);
            while (number > 0)
            {
                result.Insert(0, ToChar(number % b, b));
                number /= b;
            }
            return (positive ? "" : "-") + result.ToString();
        }

        private static string ToChar(int i, int b)
        {
            if (i < 10) return i.ToString();
            if (b > 36) throw new ArgumentException("b cannot be higher than 36");
            return ((char)((i - 10) + 'a')).ToString();
        }
    }
}
